from interpreter import Interpreter
from interpreter import Parser
from interpreter import tokens, Token, TokenType
from interpreter import Lexer
from interpreter import NoOp, BinOp, Variable, Number, Node

if __name__ == "__main__":
    parser = Parser()
    interpreter = Interpreter()
    print("--- Example 1 ---")
    print(interpreter.interpret(parser.parse("""BEGIN
END.""")))
    print("--- Example 2 ---")
    print(interpreter.interpret(parser.parse("""BEGIN
	x:= 2 + 3 * (2 + 3);
        y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));
END.""")))
    print("--- Example 3 ---")
    print(interpreter.interpret(parser.parse("""BEGIN
    y := 2;
    BEGIN
        a := 3;
        a := a;
        b := 10 + a + 10 * y / 4;
        c := a - b
    END;
    x := 11;
END.
""")))




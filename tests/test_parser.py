import pytest
from interpreter import Lexer
from interpreter import Parser, ParserException,  LexerException
from interpreter import NoOp, BinOp, Variable, Number


class TestParser:
    def test_basic_programs(self):
        parser = Parser()
        ast = parser.parse("""BEGIN
END.""")
        assert str(ast.list[0]) == str(NoOp())
        ast = parser("""BEGIN
END.""")
        assert str(ast.list[0]) == str(NoOp())

        with pytest.raises(ParserException):
            parser.parse("""BEGIN
END""")

        ast = parser.parse("""BEGIN
    a := --1;
    b := +2;
END.""")
        assert str(ast.list[0]) == "BinOp:=(Variable, UnaryOp-(UnaryOp-(Number(<TokenType.FLOAT, 1>))))"
        assert str(ast.list[1]) == "BinOp:=(Variable, UnaryOp+(Number(<TokenType.FLOAT, 2>)))"
        assert str(ast.list[2]) == str(NoOp())

    def test_complex_program(self):
        parser = Parser()
        ast = parser.parse("""BEGIN
    y := 2;
    BEGIN
        a := 3;
        a := a;
        b := 10 + a + 10 * y / 4;
        c := a - b
    END;
    x := 11;
END.""")

        assert str(ast.list[0]) == "BinOp:=(Variable, Number(<TokenType.FLOAT, 2>))"
        assert str(ast.list[1].list[0]) == "BinOp:=(Variable, Number(<TokenType.FLOAT, 3>))"
        assert str(ast.list[1].list[1]) == "BinOp:=(Variable, Variable)"
        assert str(ast.list[1].list[2]) == "BinOp:=(Variable, BinOp+(BinOp+(Number(<TokenType.FLOAT, 10>), Variable), BinOp/(BinOp*(Number(<TokenType.FLOAT, 10>), Variable), Number(<TokenType.FLOAT, 4>))))"
        assert str(ast.list[1].list[3]) == "BinOp:=(Variable, BinOp-(Variable, Variable))"

        assert str(ast.list[2]) == "BinOp:=(Variable, Number(<TokenType.FLOAT, 11>))"
        assert str(ast.list[3]) == str(NoOp())

    def test_program_with_complex_statement(self):
        parser = Parser()
        parser.parse("""BEGIN
	x:= 2 + 3 * (2 + 3);
        y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));
END.""")

        with pytest.raises(ParserException):
            parser.parse("""BEGIN
            	x:= 2 + 3 * (2 + 3)
                    y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));
            END.""")

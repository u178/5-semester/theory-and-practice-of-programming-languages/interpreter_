import pytest
from interpreter import Lexer, LexerException
from interpreter.tokens import Token, TokenType


class TestLexer:
    def test_basic_math(self):
        lexer = Lexer()
        lexer.init("2 + 3 * 4")
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '2'))
        assert str(lexer.next()) == str(Token(TokenType.PLUS, '+'))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '3'))
        assert str(lexer.next()) == str(Token(TokenType.MUL, '*'))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '4'))

        assert str(lexer.next()) == str(Token(TokenType.EOS, "None"))

        lexer.init("-2.a")
        assert str(lexer.next()) == str(Token(TokenType.MINUS, '-'))
        with pytest.raises(LexerException):
            lexer.next()

    def test_advanced_math(self):
        lexer = Lexer()
        lexer.init("(2.0 / (1 -- 1))")

        assert str(lexer.next()) == str(Token(TokenType.LPAREN, '('))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '2.0'))
        assert str(lexer.next()) == str(Token(TokenType.DIV, '/'))
        assert str(lexer.next()) == str(Token(TokenType.LPAREN, '('))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '1'))
        assert str(lexer.next()) == str(Token(TokenType.MINUS, '-'))
        assert str(lexer.next()) == str(Token(TokenType.MINUS, '-'))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '1'))
        assert str(lexer.next()) == str(Token(TokenType.RPAREN, ')'))
        assert str(lexer.next()) == str(Token(TokenType.RPAREN, ')'))

        assert str(lexer.next()) == str(Token(TokenType.EOS, "None"))

        lexer.init("1 ^ 5")
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, "1"))
        assert str(lexer.next()) == str(Token(TokenType.POW, "^"))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, "5"))
        assert str(lexer.next()) == str(Token(TokenType.EOS, "None"))

        lexer.init("_")
        with pytest.raises(LexerException):
            lexer.next()




    def test_basic_program(self):
        lexer = Lexer()
        lexer.init("""BEGIN
END.""")
        assert str(lexer.next()) == str(Token(TokenType.BEGIN, 'BEGIN'))
        assert str(lexer.next()) == str(Token(TokenType.END, 'END'))
        assert str(lexer.next()) == str(Token(TokenType.EOP, '.'))

        assert str(lexer.next()) == str(Token(TokenType.EOS, "None"))

        lexer.init(":")
        with pytest.raises(LexerException):
            lexer.next()

    def test_complex_program(self):
        lexer = Lexer()
        lexer.init("""BEGIN
    y := 2;
    BEGIN
        a := 3;
        a := y;
    END;
    x := 11;
END.""")

        assert str(lexer.next()) == str(Token(TokenType.BEGIN, 'BEGIN'))

        assert str(lexer.next()) == str(Token(TokenType.VAR, 'y'))
        assert str(lexer.next()) == str(Token(TokenType.ASSIGNMENT, ':='))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '2'))
        assert str(lexer.next()) == str(Token(TokenType.EOL, ';'))

        # nested program begins
        assert str(lexer.next()) == str(Token(TokenType.BEGIN, 'BEGIN'))
        assert str(lexer.next()) == str(Token(TokenType.VAR, 'a'))
        assert str(lexer.next()) == str(Token(TokenType.ASSIGNMENT, ':='))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '3'))
        assert str(lexer.next()) == str(Token(TokenType.EOL, ';'))

        assert str(lexer.next()) == str(Token(TokenType.VAR, 'a'))
        assert str(lexer.next()) == str(Token(TokenType.ASSIGNMENT, ':='))
        assert str(lexer.next()) == str(Token(TokenType.VAR, 'y'))
        assert str(lexer.next()) == str(Token(TokenType.EOL, ';'))

        assert str(lexer.next()) == str(Token(TokenType.END, 'END'))
        assert str(lexer.next()) == str(Token(TokenType.EOL, ';'))
        # nested program ends

        assert str(lexer.next()) == str(Token(TokenType.VAR, 'x'))
        assert str(lexer.next()) == str(Token(TokenType.ASSIGNMENT, ':='))
        assert str(lexer.next()) == str(Token(TokenType.FLOAT, '11'))
        assert str(lexer.next()) == str(Token(TokenType.EOL, ';'))

        assert str(lexer.next()) == str(Token(TokenType.END, 'END'))
        assert str(lexer.next()) == str(Token(TokenType.EOP, '.'))

        assert str(lexer.next()) == str(Token(TokenType.EOS, "None"))
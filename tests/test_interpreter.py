import pytest
from interpreter import Interpreter, InterpreterException
from interpreter import Parser
from interpreter import Node
from interpreter import Token, TokenType


class TestInterpreter:
    def test_basic_programs(self):
        interpreter = Interpreter()
        parser = Parser()
        assert {} == interpreter.interpret(parser.parse("""BEGIN
            END."""))

        assert {'a': 10.0} == interpreter.interpret(parser.parse("""BEGIN
        a := 10;
                END."""))

        assert {'a': 12.0, 'b': 1.0} == interpreter.interpret(parser.parse("""BEGIN
            a := --12;
            b := +1;
                    END."""))

        assert {'a': 12.0, 'b': 12.0} == interpreter.interpret(parser.parse("""BEGIN
                a := --12;
                b := a;
                        END."""))

        assert {'x': 17.0, 'y': 11.0} == interpreter.interpret(parser.parse("""BEGIN
                    x:= 2 + 3 * (2 + 3);
                    y:= 2 / 2 - 2 + 3 * ((1 + 1) + (1 + 1));
                        END."""))

        assert "interpreter. data: {'x': 17.0, 'y': 11.0}" == str(interpreter)

        with pytest.raises(KeyError):
            interpreter.interpret(parser.parse("""BEGIN
                            x:= y;
                                END."""))

        def test_complex_program():
            interpreter = Interpreter()
            parser = Parser()
            assert {} == interpreter.interpret(parser.parse("""BEGIN
        y := 2;
        BEGIN
            a := 3;
            a := a;
            b := 10 + a + 10 * y / 4;
            c := a - b
        END;
        x := 11;
    END."""))

    def test_broken_parser(self):
        # changing Node
        parser = Parser()
        interpreter = Interpreter()
        ast = parser.parse("""BEGIN
        END.""")
        ast.list[0] = Node()
        with pytest.raises(InterpreterException):
            interpreter.interpret(ast)

        # changing UnOp token
        ast = parser.parse("""BEGIN
        a := -10
        END.""")
        ast.list[0].right.op = Token(TokenType.FLOAT, '2')
        with pytest.raises(InterpreterException):
            interpreter.interpret(ast)


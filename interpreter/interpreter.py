from .tokens import TokenType, Token
from .node import BinOp, Node, Number, UnaryOp, ASTList, NoOp, Variable, Assignment
import operator


class InterpreterException(Exception):
    pass


class Interpreter:

    def __init__(self):
        self.d = None

    def __str__(self):
        return f"interpreter. data: {self.d}"

    def interpret(self, tree: Node) -> dict:
        self.d = {}
        self._visit(tree)
        return self.d

    def _visit(self, node: Node):

        if isinstance(node, ASTList):
            return self._visit_ASTLIST(node)
        elif isinstance(node, Assignment):
            return self._visit_assignment(node)
        elif isinstance(node, Variable):
            return self._visit_variable(node)
        elif isinstance(node, NoOp):
            return self._visit_noop(node)
        elif isinstance(node, UnaryOp):
            return self._visit_unop(node)
        elif isinstance(node, Number):
            return self._visit_number(node)
        elif isinstance(node, BinOp):
            return self._visit_binop(node)
        #  currently, unreachable. In case of adding new Nodes or breaking Parser
        raise InterpreterException("invalid node")

    def _visit_ASTLIST(self, node: ASTList):
        for l in node.list:
            self._visit(l)

    def _visit_assignment(self, node: Assignment):
        self.d[node.left.value] = self._visit(node.right)

    def _visit_variable(self, node: Variable):
        a = self.d[node.value]
        if a is None:
            pass
        return a

    def _visit_number(self, node: Node) -> float:
        return float(node.token.value)

    def _visit_noop(self, node):
        pass

    def _visit_binop(self, node: Node):
        op = node.op

        binop = {TokenType.PLUS: operator.add,
                 TokenType.MINUS: operator.sub,
                 TokenType.DIV: operator.truediv,
                 TokenType.MUL: operator.mul}.get(op.type_)

        if binop:
            return binop(self._visit(node.left), self._visit(node.right))

    def _visit_unop(self, node: Node):
        op = node.op

        if op.type_ == TokenType.PLUS:
            return self._visit(node.node)
        if op.type_ == TokenType.MINUS:
            return -self._visit(node.node)
        #  currently, unreachable. In case of adding new unary ops into the Parser
        raise InterpreterException("invalid operator")
